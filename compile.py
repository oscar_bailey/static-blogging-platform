# -*- coding: utf-8 -*-
import os
import shutil
import time
import markdown
import math
import codecs
from jinja2 import Environment, PackageLoader, FileSystemLoader

# Define custom settings
posts_per_page = 5
# Define paths
current_path = os.path.dirname(os.path.realpath(__file__))
template_path = os.path.join(current_path, 'templates')
blog_path = os.path.join(current_path, 'blog')
html_path = os.path.join(current_path, 'blog_HTML')
other_html_path = os.path.join(current_path, 'other_HTML')
# Setup template environment
env = Environment(loader=FileSystemLoader(template_path))

class Post:
	def __init__(self, title, time_created, content):
		self.title = title
		self.time_created = time_created
		self.content = content

class Page:
	def __init__(self, total_pages, current_page, visible_pages, left_arrow, right_arrow):
		self.total_pages = total_pages
		self.current_page = current_page
		self.visible_pages = visible_pages
		self.left_arrow = left_arrow
		self.right_arrow = right_arrow

def get_filenames():
	filenames = [os.path.join(blog_path,x) for x in os.listdir(blog_path)]
	return filenames

def compile():
	global posts_per_page
	posts_per_page = float(posts_per_page)
	# Sort filenames according to time created.
	filenames = sorted(get_filenames(), key=lambda filename: os.path.getctime(filename))
	posts = []
	for post_path in filenames:
		title = os.path.basename(post_path).split('.')[0]
		content = markdown.markdown(open(post_path).read())
		time_created = time.ctime(os.path.getctime(post_path))
		# Create a Post class for every post and add it to a list.
		newPost = Post(title,time_created,content)
		posts.append(newPost)
	
	# Setup multiple pages
	template = env.get_template('index.html')
	total_pages = int(math.ceil(len(posts) / posts_per_page))
	posts_per_page = int(posts_per_page)
	pages=[]
	# Setup for 10 pages displayed on every page
	for page in range(total_pages+1)[-total_pages:]:
		if page == total_pages:
			display_posts = posts[(total_pages-1)*posts_per_page:]
		else:
			display_posts = posts[(page-1)*posts_per_page:page*posts_per_page]
		if page == 1:
			pagename = 'index.html'
		else:
			pagename = 'page' + str(page) + '.html'
		left_arrow = False
		right_arrow = False
		if total_pages < 10:
			# List all pages, no arrows
			page_range = range(1,total_pages+1) 
		elif page-5 < 1:
			# List until page 10, right arrow
			page_range = range(1,11)
			right_arrow = True
		elif total_pages > page + 5:
			# List pages around current page, both arrows
			page_range = range(page-4,page+6)
			left_arrow = True
			right_arrow = True
		else:
			# List pages 10 before last page, left arrow
			page_range = range(total_pages-9,total_pages+1)
			left_arrow = True

		current_page = page
		page = Page(total_pages, current_page, page_range, left_arrow, right_arrow)
		with codecs.open(os.path.join(html_path, pagename),'w','utf-8') as index_file:
			index_file.write(template.render(posts=display_posts, page=page))

	# Copy files in other_HTML to the blog_HTML folder
	files_to_copy = os.listdir(other_html_path)
	for filename in files_to_copy:
		src_file = os.path.join(other_html_path,filename)
		dst_file = os.path.join(html_path,filename)
		shutil.copyfile(src_file, dst_file)

	print "Compile Conplete!"

if __name__ == "__main__":
	print "Beginning compilation..."
	compile()