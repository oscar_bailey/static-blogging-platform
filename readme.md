Oscar's Static CMS
==================

MD --> HTML Blog

Requirements
------------

* [Python Markdown](https://pypi.python.org/pypi/Markdown)
* [Jinja](https://pypi.python.org/pypi/Jinja2)

Usage
-----

* Put blog posts in Markdown format into the blog folder, named <title of post>.md (file extension doesn't matter)
* Run compile.py
* Copy contents of blog_HTML to webserver

File tree
---------
**Make sure to have this structure before running compile.py**

>/blog -- Put markdown blogposts here
>
>/blog_HTML -- Copy this to webserver after compiling
>
>/other_HTML -- Put other stuff to put in blog_HTML in here (e.g. style.css)0
>
>/templates -- Put templates here (index.html, post.html)
>
>compile.py

Things to note
--------------

The posts will be dated from the date the md files were CREATED, not the date modified.